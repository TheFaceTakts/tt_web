# coding: utf-8

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='profile',
                                verbose_name=u'Пользователь')
    avatar = models.ImageField(verbose_name=u'Аватарка')

    class Meta:
        verbose_name = u'Профиль пользователя'
        verbose_name_plural = u'Профили пользователей'

    def __str__(self):
       return self.user.username

# Create your models here.

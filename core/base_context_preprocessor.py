from . import forms
def login_form_processor(request):
    form = forms.LoginForm()
    return {'login_form': form}

# coding: utf-8
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout
from crispy_forms.bootstrap import FormActions

class LoginForm(forms.Form):
    username = forms.CharField(label='', widget=forms.TextInput(
            attrs={'placeholder': 'username'}
        )
    )
    password = forms.CharField(label='', widget=forms.PasswordInput(
            attrs={'placeholder': 'password'}
        )
    )

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-inline'
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.add_input(Submit('submit', 'Log in'))
        #self.helper.add_input(Submit('submit', 'Изменить порядок'))

from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import login, logout
from . import views

urlpatterns = [
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout'),

    url(r'^questions/', include('questions.urls', namespace='questions')),
    url(r'^answers/', include('answers.urls', namespace='answers')),
    url(r'^ratingchange/', views.update_rating, name='ratingchange'),
    url(r'^$', views.index, name='index'),
]

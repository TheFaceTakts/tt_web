from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
import answers.models
import questions.models
# Create your views here.

def update_rating(request):
    if (request.user.is_authenticated()):
        type = request.POST.get("type")
        id = request.POST.get("number")
        print(type, id)
        if (type == "answer"):
            try:
                answer = answers.models.Answer.objects.get(pk=int(id))
                if  not answer.ratingUsers.all().filter(pk=request.user.pk):
                    answer.ratingUsers.add(request.user)
                answer.rating = answer.ratingUsers.count();
                answer.save()
                return JsonResponse({'rating' : answer.ratingUsers.count()})
            except Exception as e:
                print(e)
        elif (type == "question"):
            try:
                question = questions.models.Question.objects.get(pk=int(id))
                if not question.ratingUsers.all().filter(pk=request.user.pk):
                    question.ratingUsers.add(request.user)
                question.rating = question.ratingUsers.count();
                question.save()
                return JsonResponse({'rating' : question.ratingUsers.count()})
            except Exception as e:
                print(e)


def index(request):
    return redirect('core:questions:all')

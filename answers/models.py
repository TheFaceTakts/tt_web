# coding=utf-8

from django.db import models
import questions
from django.conf import settings
from django.db.models import Count
from django.core.urlresolvers import reverse

# Create your models here.

class AnswerQuerySet(models.QuerySet):
    def add_rating(self):
        qs = self.annotate(rating2=Count('ratingUsers'))
        return qs

class Answer(models.Model):
    body = models.TextField(verbose_name=u'Текст ответа')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Автор')
    rating = models.IntegerField(verbose_name=u'Рейтинг')
    ratingUsers = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                         blank=True,
                                         verbose_name=u'Лайкнувшие',
                                         related_name='likedAnswer')
    question = models.ForeignKey(questions.models.Question,
                                 related_name='answers',
                                 verbose_name=u'Вопрос')

    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=u'Время создания')

    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name=
                                          u'Время последнего изменения')

    def as_compact_dict(self, user):
        showLike = (user.is_authenticated()
                    and (not user in self.question.ratingUsers.all()))
        print(self.author, user, self.author == user)
        return {
                'pk' : self.pk,
                'body': self.body,
                'author': str(self.author),
                'edit': self.author == user,
                'editUrl': reverse('core:answers:edit', args=[self.pk]),
                'rating' : self.rating,
                'created_at': self.created_at.strftime('%H:%M %Y-%m-%d'),
                'showLike' : showLike,
                'user-avatar-url' : self.author.profile.avatar.url,
        }
    class Meta:
        verbose_name = u'Ответ'
        verbose_name_plural = u'Ответы'
        ordering = ['-rating']
    def __str__(self):
       return self.body[:100]

    objects = AnswerQuerySet.as_manager()

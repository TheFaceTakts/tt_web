# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-04-06 18:28
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('answers', '0007_answer_ratingusers'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='ratingUsers',
            field=models.ManyToManyField(blank=True, related_name='likedAnswer', to=settings.AUTH_USER_MODEL, verbose_name='Лайкнувшие'),
        ),
    ]

from django.conf.urls import url, include
from django.contrib import admin
from . import views
urlpatterns = [
    url(r'^edit/(?P<pk>\d+)/$', views.AnswerEdit.as_view(), name='edit'),
]

from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.views.generic.edit import CreateView, UpdateView
from . import models
# Create your views here.

class AnswerEdit(UpdateView):
    model = models.Answer
    fields = ['body']

    def get_success_url(self):
        return reverse('core:questions:question', args=[self.question.pk])

    def dispatch(self, request, pk=None, *args, **kwargs):
        self.question = models.Answer.objects.get(pk=pk).question
        return super(AnswerEdit, self).dispatch(request, *args, **kwargs)

from django.core.management import BaseCommand
from ...models import Question, Tag
from io import BytesIO
import gzip
from urllib.request import urlopen
import random
import json

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        questions = list(Question.objects.all())
        random.shuffle(questions)
        questions = questions[:10]
        tags = list(Tag.objects.all())
        for i in questions:
            i.tags.add(random.choice(tags))
            i.save()

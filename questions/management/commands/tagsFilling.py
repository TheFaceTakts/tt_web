from django.core.management import BaseCommand
from ...models import Tag
from io import BytesIO
import gzip
from urllib.request import urlopen
import random
import json

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        page_number = random.randint(1, 400)
        byte_page = urlopen('https://api.stackexchange.com/2.2/tags?page='
                            + str(page_number)
                            + '&pagesize=100&order=desc&sort=popular'
                            + '&site=stackoverflow').read()
        names = json.loads(gzip.GzipFile(fileobj=BytesIO(byte_page)).read()
                    .decode("utf-8"))["items"]
        for i in names:
            tag = Tag()
            tag.title = i["name"]
            tag.save()

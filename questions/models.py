# coding=utf-8

from django.db import models
from django.contrib.auth.models import User

from django.conf import settings
from django.db.models import Count
# Create your models here.

class QuestionQuerySet(models.QuerySet):
    def add_rating(self):
        qs = self.annotate(rating2=Count('ratingUsers'))
        print(qs)
        return qs

class Question(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'Заголовок')
    body = models.TextField(verbose_name=u'Тект вопроса')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Автор')
    rating = models.IntegerField(verbose_name=u'Рейтинг')
    ratingUsers = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                         blank=True,
                                         verbose_name=u'Лайкнувшие',
                                         related_name='liked')

    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=u'Время создания')

    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name=
                                          u'Время последнего изменения')
    def get_cent_answers_channel_name(self):
        return "questionChannel" + str(self.id)
    
    class Meta:
        verbose_name = u'Вопрос'
        verbose_name_plural = u'Вопросы'
        ordering = ['-rating']
    def __str__(self):
       return self.title[:50]

    objects = QuestionQuerySet.as_manager()

class Tag(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'Название')

    questions = models.ManyToManyField(Question, blank=True,
                                       related_name='tags',
                                       verbose_name=u'Вопросы')

    class Meta:
        verbose_name = u'Тэг'
        verbose_name_plural = u'Тэги'

    def __str__(self):
       return self.title[:50]

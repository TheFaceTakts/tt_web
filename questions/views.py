from django.shortcuts import render, redirect, get_object_or_404
from . import models
from . import forms
import answers.models
import core.forms
from django.core.urlresolvers import reverse
from django.db.models import Count
# Create your views here.

from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView

def add_sort_method_to_self(self, request):
    sort_method_form = forms.QuestionsSortForm(request.GET)

    if (sort_method_form.is_valid()):
        self.sort_method = sort_method_form.data.get('sort_field')
        self.filter_words = sort_method_form.data.get('filter_words')
    else:
        self.sort_method = None
        self.filter_words = None

def sort_queryset(qs, method):
    if (method):
        qs = qs.order_by(method)
    return qs

class QuestionsList(ListView):
    model = models.Question
    template_name = "questions/all.html"

    def dispatch(self, request, *args, **kwargs):
        add_sort_method_to_self(self, request)
        return super(QuestionsList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = sort_queryset(self.model.objects.all(), self.sort_method)
        if (self.filter_words):
            queryset = queryset.filter(title__icontains=self.filter_words)
        queryset = queryset.add_rating()
        return queryset

    def get_context_data(self, **kwargs):
        context = super(QuestionsList, self).get_context_data(**kwargs)

        context['sort_form'] = forms.QuestionsSortForm(initial=
            {
                'sort_field': self.sort_method,
                'filter_words': self.filter_words,
            }
        )
        return context


class QuestionDetail(CreateView):
    form_class = forms.NewCommentForm
    model = answers.models.Answer
    template_name = "questions/question.html"

    def get_success_url(self):
        answer = self.object
        from adjacent import Client
        client = Client()
        client.publish(answer.question.get_cent_answers_channel_name(),
                       answer.as_compact_dict(self.user))
        response = client.send()
        print(str(answer))
        print('sent to channel {}, got response from centrifugo: {}'
                .format(answer.question.get_cent_answers_channel_name(),
                        response))
        return self.request.path

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.question = self.question
        form.instance.rating = 0
        return super(QuestionDetail, self).form_valid(form)

    def dispatch(self, request, pk=None, *args, **kwargs):
        self.user = request.user
        self.question = get_object_or_404(models.Question, id=pk)
        sort_method_form = forms.AnswersSortForm(request.GET)

        if (sort_method_form.is_valid()):
            self.sort_method = sort_method_form.data.get('sort_field')
        else:
            self.sort_method = None

        print(self.question)
        return super(QuestionDetail, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(QuestionDetail, self).get_context_data(**kwargs)
        context['question'] = self.question
        context['answers'] = sort_queryset(self.question.answers.all(),
                                     self.sort_method)
        context['sort_form'] = forms.AnswersSortForm(initial=
            {'sort_field': self.sort_method}
        )
        context['ordering'] = self.sort_method
        return context

class QuestionAnswers(ListView):
    model = answers.models.Answer
    template_name = "questions/answers.html"

    def dispatch(self, request, pk=None, *args, **kwargs):
        self.question = get_object_or_404(models.Question, id=pk)
        sort_method_form = forms.AnswersSortForm(request.GET)

        if (sort_method_form.is_valid()):
            self.sort_method = sort_method_form.data.get('sort_field')
        else:
            self.sort_method = None

        print(self.question)
        return super(QuestionAnswers, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super(QuestionAnswers, self).get_queryset()
        return queryset

    def get_context_data(self, **kwargs):
        context = super(QuestionAnswers, self).get_context_data(**kwargs)

        context['question'] = self.question

        context['answers'] = sort_queryset(self.question.answers.all(),
                                     self.sort_method).add_rating()
        return context

class QuestionCreate(CreateView):
    model = models.Question
    template_name = 'questions/new.html'
    form_class = forms.NewQuestionForm

    def get_success_url(self):
        return reverse('core:questions:all')

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.rating = 0
        return super(QuestionCreate, self).form_valid(form)


class QuestionEdit(UpdateView):
    model = models.Question
    #fields = ['title', 'body']
    form_class = forms.NewQuestionForm

    def get_success_url(self):
        return reverse('core:questions:question', args=[self.pk])

    def dispatch(self, request, pk=None, *args, **kwargs):
        self.pk = pk
        return super(QuestionEdit, self).dispatch(request, *args, **kwargs)

# coding: utf-8
from django import forms

from django.forms import ModelForm, Textarea
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout
from crispy_forms.bootstrap import FormActions

from answers.models import Answer
from .models import Question, Tag

class QuestionsSortForm(forms.Form):
    SORT_CHOICES = (
        ('-rating', u'Рейтингу'),
        ('-updated_at', u'Дате последнего изменения'),
        ('-created_at', u'Дате создания'),
    )
    sort_field = forms.ChoiceField(choices = SORT_CHOICES,
                                   label = u'Сортировать по')
    filter_words = forms.CharField(label=u'Содержат')
    def __init__(self, *args, **kwargs):
        super(QuestionsSortForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.disable_csrf = True
        self.helper.form_class = 'form-inline'
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Изменить порядок'))

class AnswersSortForm(forms.Form):
    SORT_CHOICES = (
        ('-rating', u'Рейтингу'),
        ('-created_at', u'Дате создания'),
        ('-updated_at', u'Дате последнего изменения'),

    )
    sort_field = forms.ChoiceField(choices = SORT_CHOICES,
                                   label = u'Сортировать по')

    def __init__(self, *args, **kwargs):
        super(AnswersSortForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.disable_csrf = True
        self.helper.form_class = 'form-inline'
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Изменить порядок'))




class NewCommentForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(NewCommentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Отправить'))

    class Meta:
        model = Answer
        fields = ['body']

class NewQuestionForm(ModelForm):

    def __init__(self, *args, **kwargs):
        # Only in case we build the form from an instance
        # (otherwise, 'toppings' list should be empty)
        if kwargs.get('instance'):
            # We get the 'initial' keyword argument or initialize it
            # as a dict if it didn't exist.
            initial = kwargs.setdefault('initial', {})
            # The widget for a ModelMultipleChoiceField expects
            # a list of primary key for the selected data.
            initial['tags'] = [t.pk for t in kwargs['instance'].tags.all()]

        super(NewQuestionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Задать вопрос'))

    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all())

    def save(self, commit=True):
        # Get the unsave Pizza instance
        instance = forms.ModelForm.save(self, False)

        # Prepare a 'save_m2m' method for the form,
        old_save_m2m = self.save_m2m
        def save_m2m():
           old_save_m2m()
           # This is where we actually link the pizza with toppings
           instance.tags.clear()
           for topping in self.cleaned_data['tags']:
               instance.tags.add(topping)
        self.save_m2m = save_m2m

        # Do we need to save all changes now?
        if commit:
            instance.save()
            self.save_m2m()

        return instance

    class Meta:
        model = Question
        fields = ['title', 'body', 'tags']

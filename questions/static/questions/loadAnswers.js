function reloadAnswers() {
    $("#answers").load($("#loader").attr("data-url"));
}


function renderAnswer(data) {
    var mainDiv = $("<div>").addClass("question")
    var ratingDiv = $("<div>").addClass("qRating")
        .append($("<p>").text(data["rating"]))
    if (data["showLike"]) {
        ratingDiv.append(
            $("<button>").addClass("ratingUp")
                        .addClass("btn")
                        .addClass("btn-success")
                        .data("type", "answer")
                        .data("number", data["pk"].toString())
                        .text("Like")
        );
    }
    mainDiv.append(ratingDiv);
    var bodyDiv
        = $("<div>")
            .addClass("qBody")
            .append($("<div>")
                .addClass("qContainer")
                .append(
                    $("<div>")
                        .addClass("qDesc")
                        .addClass("text-center")
                        .text(data["body"])
                )
                .append(
                    $("<div>")
                        .addClass("qAuthor")
                        .append(
                            $("<div>")
                                .text(data["created_at"])
                        )
                        .append(
                            $("<span>")
                                .addClass("colored")
                                .text(data["author"])
                        )
                        .append(
                            $("<img>")
                                .attr("src", data["user-avatar-url"])
                                .attr("height", "40px")
                                .addClass("img-circle")
                        )
                )
            );
    if (data["edit"]) {
        bodyDiv.append(
            $("<a>")
                .attr("href", data["editUrl"])
                .text("Редактировать")
        );
    }
    mainDiv.append(bodyDiv);

    return mainDiv;
}

function subscribe_to_new_answers() {
  console.log("in");
  var $info = $('#answers-cent-data');
  var centrifuge = new Centrifuge({
    url: $info.data('cent-url'),
    user: $info.data('cent-user').toString(),
    timestamp: $info.data('cent-ts').toString(),
    info: $info.data('cent-info'),
    token: $info.data('cent-token'),
    debug: false,
  });

  var channel = $info.data('cent-channel');
  console.log('subscribe on channel ' + channel);
  centrifuge.subscribe(channel, function(msg) {
    console.log(msg);
    $('#answers').append("<hr>").append(renderAnswer(msg.data));
  });
  centrifuge.connect();
  return centrifuge;
}

var g_centrifuge = undefined;

$(document).ready(function() {
    console.log("ready");
    reloadAnswers();
    if (g_centrifuge === undefined) {
        g_centrifuge = subscribe_to_new_answers();
    }

    //window.setInterval(reloadAnswers, 5000);
});

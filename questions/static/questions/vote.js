    $(document).ready(function() {
        $(document).on("click", ".ratingUp", function() {
            var me = $(this);
            var sibling = $($(this).prev());
            var current = parseInt(sibling.html());
            $.ajax({
                type: "POST",
                url: $("#ajax").data("url"),
                data: {
                    'type' : $(this).data("type"),
                    'number' : $(this).data("number"),
                    'csrfmiddlewaretoken': $("#ajax").data("csrf"),
                },
                success: function(data) {
                    console.log(data);
                    me.css("visibility", "hidden");
                    console.log(sibling.html());
                    sibling.html(data["rating"]);
                },
            });
        });
    });

from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^all/$', views.QuestionsList.as_view(), name='all'),
    url(r'^new/$', views.QuestionCreate.as_view(), name='create'),
    url(r'^edit/(?P<pk>\d+)/$', views.QuestionEdit.as_view(), name='edit'),
    url(r'^question/(?P<pk>\d+)/$', views.QuestionDetail.as_view(), name='question'),
    url(r'^answers/(?P<pk>\d+)/$', views.QuestionAnswers.as_view(), name='answers'),
]
